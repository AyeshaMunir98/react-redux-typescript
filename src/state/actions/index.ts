import { ActionType } from '../action-types'

interface DepositAction {
    type: ActionType.DEPOSIT
    payload: number
}

interface WithdrawAction{
    type: ActionType.WITHDRAW
    payload: number
}

interface BankruptAction{
    type: ActionType.BANKRUPT//dont have payload in this case
}

export type Action = DepositAction | WithdrawAction | BankruptAction 
